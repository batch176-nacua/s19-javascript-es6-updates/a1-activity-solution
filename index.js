const getCube = 2;
console.log(`The cube of ${getCube} is ${getCube ** 3}`)

const address = ['44b', 'M.Velez','Cebu City']
const [numberAdd, streetAdd, streetCity] = address;
console.log(`I live at ${numberAdd} ${streetAdd} ${streetCity}`)

const animals = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: 1075,
	length: '20 ft 3 in'
}

const {name, type, weight, length} = animals;
console.log(`${name} was a ${type}. He weighted at ${weight} kgs with a measurement of ${length}.`)

const arrNum = [1, 2, 3, 4, 5]
arrNum.forEach((x) => console.log(x))

class Dog{
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}

const dog1 = new Dog("Buddy", 7, "Highbred")
console.log(dog1)